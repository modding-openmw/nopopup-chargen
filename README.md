# NoPopUp Chargen

Removes all tutorial pop-ups from chargen. Works with OpenMW and Morrowind.exe/MGE/MWSE.

#### Credits

**Author**: johnnyhostile

**Special Thanks**:

* The OpenMW team, including every contributor (for making OpenMW and OpenMW-CS)
* All the users in the `modding-openmw-dot-com` Discord channel on the OpenMW server for their dilligent testing <3
* Bethesda for making Morrowind

And a big thanks to the entire OpenMW and Morrowind modding communities! I wouldn't be doing this without all of you.

#### Web

[Project Home](https://modding-openmw.gitlab.io/nopopup-chargen/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/nopopup-chargen)

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/nopopup-chargen/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Chargen\nopopup-chargen

        # Linux
        /home/username/games/OpenMWMods/Chargen/nopopup-chargen

        # macOS
        /Users/username/games/OpenMWMods/Chargen/nopopup-chargen

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Chargen\nopopup-chargen"`)
1. Add `content=nopopup=chargen.esp` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher

Also included is a compatibility patch for a compatibility patch for [Quest Voice Greetings](https://www.nexusmods.com/morrowind/mods/52273). When using it with this mod, be sure your load order is something like this:

```
...

content=nopopup-chargen.esp
content=Quest Voice Greetings.ESP
content=nopopup-chargen-qvg-patch.esp
...
```

#### Connect

* [Discord](https://discord.gg/KYKEUxFUsZ)
* [IRC](https://web.libera.chat/?channels=#momw)
* File an issue [on GitLab](https://gitlab.com/modding-openmw/nopopup=chargen/-/issues/new) for bug reports or feature requests
* Email: `johnnyhostile at modding-openmw dot com`
<!-- * Leave a comment [on Nexus mods](https://www.nexusmods.com/morrowind/mods/#TODO?tab=posts) -->
