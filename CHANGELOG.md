## NoPopUp Chargen Changelog

#### 1.4

* Recompiled scripts changed in the last release so that they work properly in Morrowind.exe

[Download Link](https://gitlab.com/modding-openmw/nopopup-chargen/-/packages/24166886)

#### 1.3

* Fixed a problem that could cause the magic menu to not enable

[Download Link](https://gitlab.com/modding-openmw/nopopup-chargen/-/packages/24165593)

#### 1.2

* Renamed the [Quest Voice Greetings](https://www.nexusmods.com/morrowind/mods/52273) patch to make it more obvious that it's for that mod.

[Download Link](https://gitlab.com/modding-openmw/nopopup-chargen/-/packages/23925225)

#### 1.1

* Added a compatibility patch for [Quest Voice Greetings](https://www.nexusmods.com/morrowind/mods/52273)

[Download Link](https://gitlab.com/modding-openmw/nopopup-chargen/-/packages/21654835)

#### 1.0

* Initial release

[Download Link](https://gitlab.com/modding-openmw/nopopup-chargen/-/packages/20240621)
