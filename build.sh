#!/bin/sh
set -e
delta_plugin convert nopopup-chargen.yaml
mv nopopup-chargen.omwaddon nopopup-chargen.esp

cd QuestVoiceGreetingsPatch
delta_plugin convert nopopup-chargen-quest-voice-greetings-patch.yaml
mv nopopup-chargen-quest-voice-greetings-patch.omwaddon ../nopopup-chargen-quest-voice-greetings-patch.esp
